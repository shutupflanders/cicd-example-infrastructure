variable "registry_user" {
  type = string
}

variable "registry_token" {
  type = string
}