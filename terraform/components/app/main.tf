# Pulls the image
resource "docker_image" "app" {
  name = "registry.gitlab.com/shutupflanders/cicd-example-codebase"
}

# Create a container
resource "docker_container" "app" {
  image = docker_image.app.latest
  name  = "app"
}