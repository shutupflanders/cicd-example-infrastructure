provider "docker" {
  host    = "unix:///var/run/docker.sock"

  registry_auth {
    address  = "registry.gitlab.com"
    username = var.registry_user
    password = var.registry_token
  }
}